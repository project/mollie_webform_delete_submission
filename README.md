# Mollie Webform Delete Submission.

Privacy regulations sometimes dictate that a website must not store contact information. A webform has a viable option to meet this requirement. Pick Disable saving of submissions in the settings. Naturally, this makes sense only the entered information is sent (for example with a webservice) to another system.

However, when Mollie is enabled, this does not work. The use of Mollie entails a two step process. First the webform is used to enter the submission, then the user is forwarded to the Mollie payments screen, and new information is generated like the payment identifier and the payment status. This information is reported back to a Drupal with a web service, and is difficult to be processed when the submission is already deleted.

This module offers an alternative. It deletes the submission after the information is reported back.

## How to use

You can add the handler _Mollie Delete Webform Submission_ to the webform. In the handler you can configure on what states the submission must be deleted.

## Technical Details

This module registers an subscriber to the `'mollie.transaction_event.status_change'` event. When the subscriber receives this event, it looks up the corresponding submission. It the webform of the submission has the __Mollie Delete Webform Submission_ the submission is deleted. Disabling the handler also prevents the delete.


