<?php
/*
 * @author Klaas Eikelboom  <klaas.eikelboom@civicoop.org>
 * @date 25-Apr-2023
 * @license  GPL-2.0-or-later
 */

namespace Drupal\mollie_webform_delete_submission\EventSubscriber;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\mollie\Events\MollieTransactionStatusChangeEvent;
use Drupal\webform\Entity\WebformSubmission;
use Drupal\webform\Plugin\WebformHandlerBase;
class MollieTransactionEventSubscriber implements \Symfony\Component\EventDispatcher\EventSubscriberInterface {

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * Constructs event subscriber.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * @return array[]
   */
  public static function getSubscribedEvents() {
    return [
      MollieTransactionStatusChangeEvent::EVENT_NAME => [
        'deleteSubmission',
        -9000
      ],
    ];
  }

  /**
   * @param \Drupal\mollie\Events\MollieTransactionStatusChangeEvent $event
   *
   * @return void
   */
  public function deleteSubmission(MollieTransactionStatusChangeEvent $event): void {
    $httpCode = 200;
    try {
      /** @var Webformsubmission $submission */
      $submission = $this->entityTypeManager->getStorage('webform_submission')
        ->load($event->getContextId());
      $webform = $submission->getWebform();
      /** @var WebformHandlerBase $deleteSubmissionHandler */
      $deleteSubmissionHandler = FALSE;
      $handlers = $webform->getHandlers();
      foreach ($handlers as $handler) {
        if ($handler->getPluginId() == 'mollie_delete_submission_handler') {
          $deleteSubmissionHandler = $handler;
        }
      }
      if($deleteSubmissionHandler && !$deleteSubmissionHandler->isDisabled()){
        $transaction = $event->getTransaction();
        $status = $transaction->getStatus();
        $states = $deleteSubmissionHandler->getConfiguration()['settings']['mollie_payment_states'] ?? ['paid','expired','failed'];
        if(in_array($status,$states)){
          $submission->delete();
        }
      }
    }
    catch (\Exception $e) {
      watchdog_exception('mollie_webform_delete_submission', $e);
      $httpCode = 500;
    }

    // Set the HTTP code to return to Mollie.
    $event->setHttpStatusCode($httpCode);
  }

}
