<?php
/*
 * @author Klaas Eikelboom  <klaas.eikelboom@civicoop.org>
 * @date 25-Apr-2023
 * @license  GPL-2.0-or-later
 */

namespace Drupal\mollie_webform_delete_submission\Plugin\WebformHandler;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformHandlerBase;
/**
 *
 * @WebformHandler(
 *   id = "mollie_delete_submission_handler",
 *   label = @Translation("Mollie Delete Webform Submission"),
 *   category = @Translation("Mollie"),
 *   description = @Translation("Deletes the webform submission when the webhook reports back with an agreed status"),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_SINGLE,
 * )
 */
class MollieDeleteSubmissionHandler extends WebformHandlerBase{

  /**
   * @return array
   */
  private function paymentStates() {
    return  [
      'open' => t('Open'),
      'canceled' => t('Canceled'),
      'pending' => t('Pending'),
      'authorized' => t('Authorized'),
      'expired' => t('Expired'),
      'failed' => t('Failed'),
      'paid' => t('Paid'),
    ];
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['mollie_delete_submission'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Mollie Delete Submission'),
      '#description' => t('According to Mollie Documentation only expired,failed and paid call the webhook')
    ];
    $form['mollie_delete_submission']['mollie_payment_states'] = [
      '#type' => 'checkboxes',
      '#title' => t('Mollie Payment Statuses'),
      '#options' => $this->paymentStates(),
      '#default_value' => $this->getConfiguration()['settings']['mollie_payment_states'] ?? ['paid','expired','failed']
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $values = $form_state->getValues();
    $this->configuration['mollie_payment_states'] = $values['mollie_delete_submission']['mollie_payment_states'];
  }

  /**
   * @return array
   */
  public function getSummary() {
    $mollie_payment_states = $this->configuration['mollie_payment_states'] ?? ['paid','expired','failed'];
    $mollie_payment_states = array_filter($mollie_payment_states, function($x){ return $x!=0 ; });
    $mollie_payment_states = array_map(function($x){ return $this->paymentStates()[$x] ; },$mollie_payment_states);
    return [
      '#markup' => $this->t('Submissions wil be deleted when Mollie reports the following statuses back : @statuses',[
        '@statuses' => implode(',',$mollie_payment_states),
      ]),
    ];
  }

}
